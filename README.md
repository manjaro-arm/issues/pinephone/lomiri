## Bugtracker for our Lomiri edition

![](https://pbs.twimg.com/media/EhgwSvuWAAAOnRg?format=jpg&name=large)

**Lomiri** offers a completely different approach to using your smartphone or tablet than using other mainstream operating systems. The OS is built on Manjaro, which provides us with a secure and stable base system.

The intuitive user interface allows you to access all of your phone's features by swiping from the edges of the screen to access your apps, tools and settings all with one hand and no on-screen buttons. It looks great and feels smooth.

Together with the [UBports Foundation](https://ubports.com/foundation/ubports-foundation) we are working on makeing **Lomiri** a great supported desktop environment on Pine64 devices.

You can download it for your device from here:

- [Download for PinePhone](https://osdn.net/projects/manjaro-arm/storage/pinephone/lomiri/)

---

If you find some issue with the software, please feel free to issue a [new bug-report](https://gitlab.manjaro.org/manjaro-arm/issues/pinephone/lomiri/-/issues/new)